def importPersonCSV(filePath,repo):
    personCsv = open(filePath, "r")

    for line in personCsv.read().split("\n")[1:]:
        tableContent = line.split(",")
        repo.addPerson(tableContent[0],tableContent[1],tableContent[2],tableContent[3],tableContent[4],tableContent[5])

def importMarriageCSV(filePath,repo):
    marriageCsv = open(filePath, "r")

    for line in marriageCsv.read().split("\n")[1:]:
        tableContent = line.split(",")
        repo.addMarriage(tableContent[0],tableContent[1],tableContent[2],tableContent[3],tableContent[4])
