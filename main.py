from FamilyTree import *
from CsvProcessor import *

repo = Repository()
importPersonCSV("person.csv", repo)
importMarriageCSV("marriage.csv", repo)

a = repo.getPersonById("0")
b = repo.getPersonById("12")
print(a.relationTo(b))
