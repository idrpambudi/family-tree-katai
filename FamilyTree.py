from copy import deepcopy

class Person:
    def __init__(self, repository, id, name, gender, birthDate, fatherId, motherId):
        self.repo = repository
        self.id = id
        self.name = name
        self.gender = gender
        self.birthDate = birthDate
        self.fatherId = fatherId
        self.motherId = motherId

        self.father = None
        self.mother = None
        self.spouse = None
        self.divorced = set()
        self.children = set()
        if fatherId != "":
            self.father = self.repo.getPersonById(fatherId)
            self.father.children.add(self)
        if motherId != "":
            self.mother = self.repo.getPersonById(motherId)
            self.mother.children.add(self)

    def getFather(self):
        return self.father

    def getMother(self):
        return self.mother

    # assumption: 1 people can only have 1 marriage at the same time
    def isMarried(self):
        return self.spouse != None

    # assumption: the endDate is not empty if and only if he/she is divorced
    #            people is divorced if all of their marriage has ended
    def isDivorced(self):
        return len(self.divorced) > 0

    def isParent(self, child):
        return child.father == self or child.mother == self

    def isStepParent(self, child):
        return child in self.getStepChildren()

    def getSpouse(self):
        return self.spouse

    def getFormerSpouse(self):
        return self.divorced

    def getParents(self):
        return {self.father, self.mother}

    def getParentsMutualChildren(self):
        return self.father.children & self.mother.children

    def getChildren(self):
        return self.children

    def getSiblings(self):
        return self.getParentsMutualChildren() - {self}

    def getSisters(self):
        return {sis for sis in self.getSiblings() if sis.gender == "female"}

    def getBrothers(self):
        return {bro for bro in self.getSiblings() if bro.gender == "male"}

    def getStepChildren(self):
        if self.isMarried():
            spouseChildren = self.getSpouse().children
            return self.children - spouseChildren
        return set()

    def getStepSiblings(self):
        totalParentsChildren = self.father.getChildren() | self.mother.getChildren()
        return totalParentsChildren - self.getParentsMutualChildren()

    def getStepSisters(self):
        return {sis for sis in self.getStepSiblings() if sis.gender == "female"}

    def getStepBrothers(self):
        return {bro for bro in self.getStepSiblings() if bro.gender == "male"}

    def getStepMother(self):
        if self.father.spouse != self.mother:
            return self.father.spouse
        return None

    def getStepFather(self):
        if self.mother.spouse != self.father:
            return self.mother.spouse
        return None

    def getParentsSiblings(self):
        return self.father.getSiblings() | self.mother.getSiblings()

    def getUncles(self):
        return {unc for unc in self.getParentsSiblings() if unc.gender == "male"}

    def getAunties(self):
        return {aun for aun in self.getParentsSiblings() if aun.gender == "female"}

    def getGrandParents(self):
        return self.father.getParents() | self.mother.getParents()

    def getGrandFathers(self):
        return {gf for gf in self.getGrandParents() if gf.gender == "male"}

    def getGrandMothers(self):
        return {gm for gm in self.getGrandParents() if gm.gender == "female"}

    def getGrandChildren(self):
        grandChildren = set()
        for child in self.children:
            grandChildren.add(child.children)
        return grandChildren

    def getCousins(self):
        cousins = set()
        for pSib in self.getParentsSiblings():
            cousins.add(pSib.children)
        return cousins

    def getNephews(self):
        nephews = set()
        for sib in self.getSiblings():
            nephews.add(sib.children)
        return nephews

    def find(self, nlpString):
        commandArr = nlpString.split("of")
        peoples = {self}
        for i in range(len(commandArr)-1, -1, -1):
            tempPeoples = set()
            command = commandArr[i]
            if "step" in command:
                if "children" in command:
                    for peo in peoples:
                        tempPeoples |= peo.getStepChildren()
                elif "sibling" in command:
                    for peo in peoples:
                        tempPeoples |= peo.getStepSiblings()
                elif "sister" in command:
                    for peo in peoples:
                        tempPeoples |= peo.getStepSisters()
                elif "brother" in command:
                    for peo in peoples:
                        tempPeoples |= peo.getStepBrothers()
                elif "mother" in command:
                    for peo in peoples:
                        tempPeoples |= peo.getStepMother()
                elif "father" in command:
                    for peo in peoples:
                        tempPeoples |= peo.getStepFather()
            elif "grand" in command:
                if "father" in command:
                    for peo in peoples:
                        tempPeoples |= peo.getGrandFathers()
                elif "mother" in command:
                    for peo in peoples:
                        tempPeoples |= peo.getGrandMothers()
                elif "parent" in command:
                    for peo in peoples:
                        tempPeoples |= peo.getGrandParents()
                elif "child" in command:
                    for peo in peoples:
                        tempPeoples |= peo.getGrandChildren()
            elif "spouse" in command:
                for peo in peoples:
                    tempPeoples |= peo.getSpouse()
            elif "husband" in command:
                for peo in peoples:
                    if peo.gender == "female":
                        tempPeoples |= peo.getSpouse()
            elif "wife" in command:
                for peo in peoples:
                    if peo.gender == "male":
                        tempPeoples |= peo.getSpouse()
            elif "parent" in command:
                for peo in peoples:
                    tempPeoples |= peo.getParents()
            elif "father" in command:
                for peo in peoples:
                    tempPeoples |= peo.getFather()
            elif "mother" in command:
                for peo in peoples:
                    tempPeoples |= peo.getMother()
            elif "child" in command:
                for peo in peoples:
                    tempPeoples |= peo.getChildren()
            elif "sister" in command:
                for peo in peoples:
                    tempPeoples |= peo.getSisters()
            elif "brother" in command:
                for peo in peoples:
                    tempPeoples |= peo.getBrothers()
            elif "uncle" in command:
                for peo in peoples:
                    tempPeoples |= peo.getUncles()
            elif "aunt" in command:
                for peo in peoples:
                    tempPeoples |= peo.getAunties()
            elif "cousin" in command:
                for peo in peoples:
                    tempPeoples |= peo.getCousins()
            elif "nephew" in command:
                for peo in peoples:
                    tempPeoples |= peo.getNephews()
            peoples = tempPeoples
        return peoples

    def relationTo(self, p2):
        nlArr = []
        arr = [[[], self]]
        checked = {self}
        while len(arr) != 0:
            current = arr.pop()
            currentPerson = current[1]
            currentArr = current[0]
            if currentPerson == p2:
                nlArr = currentArr
                break

            if currentPerson.getFather() not in checked:
                if currentPerson.getFather() !=None:
                    arr.append([currentArr+["father"], currentPerson.getFather()])
                    checked.add(currentPerson.getFather())

            if currentPerson.getMother() not in checked:
                if currentPerson.getMother() != None:
                    arr.append([currentArr+["mother"], currentPerson.getMother()])
                    checked.add(currentPerson.getMother())

            if currentPerson.getSpouse() not in checked:
                if currentPerson.isMarried():
                    if currentPerson.gender == "male":
                        arr.append([currentArr+["wife"], currentPerson.getSpouse()])
                    else:
                        arr.append([currentArr+["husband"], currentPerson.getSpouse()])
                    checked.add(currentPerson.getSpouse())

            for child in currentPerson.getChildren():
                if child not in checked:
                    if child.gender == "male":
                        arr.append([currentArr+["son"], child])
                    else:
                        arr.append([currentArr+["daughter"], child])
                    checked.add(child)

        if len(nlArr) == 0:
            return p2.name + " is unrealated to " + self.name

        nlStr = ""
        for relation in nlArr:
            nlStr += relation + " of "
        return p2.name + " is " + nlStr + self.name




# 1 people can only marry 1 other people at the same time, but can have more than 1 former spouse
class Marriage:
    def __init__(self, repository, id, husbandId, wifeId, startDate, endDate):
        self.repo = repository
        self.id = id
        self.husbandId = husbandId
        self.wifeId = wifeId
        self.startDate = startDate
        self.endDate = endDate
        if self.isEnded():
            self.getHusband().divorced.add(self.getWife())
            self.getWife().divorced.add(self.getHusband())
        else:
            self.getHusband().spouse = self.getWife()
            self.getWife().spouse = self.getHusband()
        # if not self.isEnded():
            # if self.repo.peoples[husbandId].children != self.repo.peoples[wifeId].children:
            #     self.repo.peoples[husbandId].stepChildren = self.repo.peoples[wifeId].children
            #     self.repo.peoples[wifeId].stepChildren = self.repo.peoples[husbandId].children

    def isEnded(self):
        if self.endDate == "":
            return False
        return True

    def getHusband(self):
        return self.repo.getPersonById(self.husbandId)

    def getWife(self):
        return self.repo.getPersonById(self.wifeId)


# assumption: will restrict add person or marriages if the ID already exist
class Repository:
    def __init__(self):
        self.peoples = {}
        self.marriages = {}

    def addPerson(self, id, name, gender, birthDate, fatherId, motherId):
        if id not in self.peoples.keys():
            self.peoples[id] = Person(self, id, name, gender, birthDate, fatherId, motherId)
        else:
            print("Person ID already exists")

    def addMarriage(self, id, husbandId, wifeId, startDate, endDate):
        if id not in self.marriages.keys():
            try:
                if endDate == "" and (self.getPersonById(husbandId).isMarried() or self.getPersonById(wifeId).isMarried()):
                    print("Husband or Wife already married")
                else:
                    self.marriages[id] = Marriage(self, id, husbandId, wifeId, startDate, endDate)
            except KeyError:
                print("Husband or Wife ID not found")
        else:
            print("Marriage ID already exists")

    def getPersonById(self, id):
        return self.peoples[id]

    def getMarriageById(self, id):
        return self.marriages[id]

    @staticmethod
    def getRelationRoot(p1, p2):
        temp1 = {p1}
        temp2 = {p2}
        ancestor1 = {p1}
        ancestor2 = {p2}
        distance = 0
        while len(ancestor1 & ancestor2 - {None}) == 0:
            if temp1 == {None} and temp2 == {None}:
                return -1
            distance += 1
            temp11 = set()
            temp22 = set()
            for people in temp1:
                ancestor1 |= people.getParents()
                temp11.add(people)
            for people in temp2:
                ancestor2 |= people.getParents()
                temp22.add(people)
            temp1 = temp11
            temp2 = temp22
        return {"root":(ancestor1 & ancestor2).pop(), "distance":distance}
